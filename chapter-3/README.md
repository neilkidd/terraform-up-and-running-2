# Terraform Up And Running, (2nd Edition) Learning - chapter 3

Chapter 3 discusses and contrasts managing multiple environments with Terrafom. This is commonly achieved via:

1. workspaces
2. File layout

The conclusion is that while workspaces can be useful for POC / prototyping style of working, file layout is superior for scaling, migration and environment isolation (via rights and per env state files), thus minimising [blast radius](https://etherealmind.com/network-dictionary-blast-radius/). Additionally, workspaces are opaque as the code structure for all environments are in one place - the structure is not self describing, requiring you to run `terraform workspace` to identify envs.

This folder uses the file layout structure used in the book along with s3 as a state store and one environment "stage".

## File Layout Benefits and best practices

* Configure a different backend for each environment
  * This could be as simple as production and pre-prod. Still suggest per env though. (staging, UAT ...) if you need them.
  * The isolation can be used to enforce separate credentials or accounts.

## Instructions

1. Provision remote state in global/s3
2. Terraform mysql in staging/data-stores/mysql
3. Terraform webserver in services/webserver-cluster
