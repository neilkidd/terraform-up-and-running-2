output "alb_dns_name" {
  value       = aws_lb.example.dns_name
  description = "The dns_name of the load balancer"
}
