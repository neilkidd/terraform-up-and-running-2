bucket         = "staging-s3-terraform-state"
region         = "us-east-1"
dynamodb_table = "staging-ddb-terraform-locks"
encrypt        = true
