
output "address" {
  value       = module.data_stores_mysql.address
  description = "Connect to the database at this endpoint"
}

output "port" {
  value       = module.data_stores_mysql.port
  description = "The port the database is listening on"
}
