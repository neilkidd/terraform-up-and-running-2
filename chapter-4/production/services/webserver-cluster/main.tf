provider "aws" {
  region = "us-east-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.48"
}

terraform {
  backend "s3" {

    # Note: backends can't use VARS.
    # use a backend.hcl file

    # This backend configuration is filled in automatically at test time by Terratest. If you wish to run this example
    # manually, uncomment and fill in the config below.

    # bucket         = "<YOUR S3 BUCKET>"
    # key            = "<SOME PATH>/terraform.tfstate"
    # region         = "us-east-1"
    # dynamodb_table = "<YOUR DYNAMODB TABLE>"
    # encrypt        = true

    key = "production/services/webserver-cluster/terraform.tfstate"
  }
}

module "webserver_cluster" {
  source = "../../../modules/services/webserver-cluster"

  cluster_name           = "production-webservers"
  ec2_instance_type      = "t2.medium"
  asg_min_size           = "2"
  asg_max_size           = "5"

  db_remote_state_bucket = "staging-s3-terraform-state"
  db_remote_state_key    = "production/data-stores/mysql/terraform.tfstate"
}
