# Terraform Up And Running, (2nd Edition) Learning

This repo contains the code created while working through the book [Terraform up and Running, 2nd Edition](https://www.terraformupandrunning.com/). The book source code can be found at [Github](https://github.com/brikis98/terraform-up-and-running-code)

I'm currently using the [Linux Academy](https://linuxacademy.com/refer?514eb3bf3cf2167073e39639c9166323) (__Referral link__) cloud playground feature to develop the code.

## Useful commands

```bash
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

### Git clean

I'm using the [Linux Academy](https://linuxacademy.com/refer?514eb3bf3cf2167073e39639c9166323) (__Referral link__) cloud playground. It's useful to clean any hangover files and state from previous sessions. These are usually the `.terraform/*` directories and files.

Add the option `n` or `--dry-run` to see what will be removed.

```bash
git clean -Xfd
```

### Terraform

[Format files](https://www.terraform.io/docs/commands/fmt.html) recursively, where `.` is the dir.

```bash
terraform fmt -recursive .
```
